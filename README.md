# Валилация форм

### INIT

git clone https://gitlab.com/AlexandrovML/form-validate.git    
or
git clone https://NAME: PASSWORD@gitlab.com/AlexandrovML/form-validate.git

1. npm i
2. npm start

### CSS

* Простые стили для формы \dev\sass\pages\form-style.scss
* Cтили для ошибок  \dev\sass\pages\form-style.scss

### JS

* Основной файл \dev\js\scripts.js

### LIBS
-CSS

``` html
<link rel="stylesheet" type="text/css" href="./libs/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="./libs/jquery-ui-1.9.2.custom.min.css">
<link rel="stylesheet" type="text/css" href="./libs/jquery.formstyler.css">
<link rel="stylesheet" type="text/css" href="./libs/jquery.formstyler.theme.css">
```

-JS

``` html
<script src="./libs/jquery.min.js"></script>
<script src="./libs/jquery-ui-1.9.2.custom.min.js"></script>
<script src="./libs/jquery.inputmask.min.js"></script>
<script src="./libs/magnific-popup.js"></script>
<script src="./libs/jquery.formstyler.min.js"></script>
```

### Общее описание:

##### Структура блока с полем для ввода данных

``` html
<div class="fv__block required">
    <label>
        <span>Заголовочный текст поля:</span>
        <input type="...">
    </label>
    <div class="fv__block-error">Поле заполнено некорректно!</div>
    <div class="fv__block-succes">Поле заполнено корректно!</div>
</div>
```

##### `class="required"`

> Делает поле обязательным для заполнения, добавляет символ * в заголовочный текст

##### Блок с классом `class="fv__block-error"`

> Данный блок нужен для вывода текстовых подсказок с текстом ошибки

##### Блок с классом `class="fv__block-succes"`

> Данный блок нужен для вывода текстовых подсказок с текстом подтверждени

##### Примечание:

> Весь функционал (обработчики событи и вызовы всплывающих окон), необходимый для корректной работы валидации помимо функций находяться в `$(document).ready(function () {}))` 

### Function()

##### popupForm_error(count)

> Функция  добавляет класс `class="error"` в блок `<div class="fv__error"></div>` , где выводим текст ошибки. В count передаем кол-во ошибок.

***
##### required_input()

> Функция используется для проверки ввода текста по событию keyup. Класс `class="required--input"` вешается на родительский элемент `<div class="fv__block"></div>` 

По дефолту стоит кол-во символов - 3, но можно менять используя аттрибут - data-length

***
##### required_email()

> Функция используется для проверки ввода емайл по событию keyup. Класс `class="required--email"` вешается на родительский элемент `<div class="fv__block">` 

***
##### emailInputControl()

> Функция используется для проверки ввода емайл по событию keyup, если поле не является обязательным. Срабатывает при вводе некорректного email - поле становится обязательным и участвует в валидации формы. Если поле для ввода пустое, то данный элемент в валидации не участвует. Класс `class="required--email"` вешается на родительский элемент `<div class="fv__block">` 

***
##### required_textarea()

> Функция используется для проверки ввода текста по событию keyup в поле textarea. Класс `class="required--textarea"` вешается на родительский элемент `<div class="fv__block">` 

***
##### num__inset()

> Функция используется для запрета ввода букв по событию keyup. Класс `class="num--inset"` вешается на родительский элемент `<div class="fv__block">` 

***
##### first_password()

> Функция используется для проверки введенных данных по событию keyup. Класс `class="password-first"` вешается на родительский элемент с тэгом `<div></div>`. При коррктном вводе появляется поле для подтверждения пароля. Если поле для подтверждеия пароля было очищено в ходе ввода данных и выполняется заполнение первого поля - то поле для подтверждения скрывается.
> При использовании данной функции необходимо объявить переменные `var = firstPasswordSucces` и `var = firstPasswordValue`.
> Блок с классом `class="fv__block-not-equally"` используется для вывода подсказки о том, что пароли не совпадают.
> Блок с классом `class="password-control"` используется для считывания события скрытия/показа текста пароля. Помещается в один элемент с `<input>`.
> Длина пароля регулируется аттрибутом maxlength="" и minlength="" `<input maxlength="4" minlength="8">`.

***
##### second_password()

> Функция используется для проверки введенных данных по событию keyup в поле для подтверждения пароля. Класс.password-second вешается на родительский элемент с тэгом `<div></div>`.
> При использовании данной функции необходимо объявить переменные secondPasswordSucces и secondPasswordValue.
> Блок с классом `class="fv__block-not-equally"` используется для вывода подсказки о том, что пароли не совпадают.
> Блок с классом `class="password-control"` используется для считывания события скрытия/показа текста пароля. Помещается в один элемент с `<input>`.
> Длина пароля регулируется аттрибутом maxlength="" и minlength="" `<input maxlength="4" minlength="8">`.

***
##### radioChange()

> Функция используется для удаление класса `class="error"` с `<input type=radio>` по событию change. Класс `class="radio--required"` вешается на родительский элемент с классом `<div class="fv__block">`.

***
##### selectChange()

> Функция используется для удаление класса `class="error"` с `<select></select>` по событию change. Класс `class="required--select"` вешается на родительский элемент с классом `<div class="fv__block">`.

***
##### checkMoreChange()

> Функция используется для удаление класса `class="error"` со всех `<input type="checkbox">` внутри `<div class="fv__block">` по событию change. Класс `class="check-more"` вешается на родительский элемент с классом `<div class="fv__block">`. Используется для множественных чекбоксов.

***
##### checkChange() 

> Функция используется для удаление класса `class="error"` с `<input type="checkbox">` по событию change. Класс `class="required--check"` вешается на родительский элемент с классом `<div class="fv__block">`.

***
##### inputMask()

> Функция используется инициализации маски по аттрибуту `<input data-mask="">`. Так же можно ставить аттрибут placeholder если добавить аттрибут data-placeholder. НЕ УЧАСТВУЕТ В ВАЛИДАЦИИ. Класс `class="input--mask"` вешается на на родительский элемент с классом `<div class="fv__block">`.

***
##### inputMaskRequired()

> Функция используется инициализации маски по аттрибуту `<input data-mask="">`. Так же можно ставить аттрибут placeholder если добавить аттрибут data-placeholder. После полного заполнения удаляется класс `class="error"` и добавляется класс `class="success"`. Класс `class="input--mask"` вешается на на родительский элемент с классом `<div class="fv__block">`.

***
##### maskInputControl()

> Функция используется для проверки ввода номера телефона по событию keyup, если поле не является обязательным. Срабатывает, если в поле началии вводить данные, но не закончили - поле становится обязательным и участвует в валидации формы. Если поле для ввода пустое, то данный элемент в валидации не участвует. Класс `class=".mask-input-control"` вешается на на родительский элемент с классом `<div class="fv__block">`.

***
##### required_date()

> Функция используется для контроля состояния поля с календарем. При ошибки добавляется класс `class="error"-online"`. Если все прошло успешно удаляется класс `class="error"` и `class="error-online"`. Класс `class="required--date"` вешается на родительский элемент с классом `<div class="fv__block">`.

***
##### fileChange() 

> Функция используется для удаление класса `class="error"` с `<input type="file">` по событию change. Класс `class="required--file"` вешается вешается на родительский элемент с классом `<div class="fv__block">`.

***
##### fileChangeMultiple() 

> Функция используется для удаление класса `class="error"` с `<input type="file" multiple>` с аттрибутом по событию change. Класс `class="required--file"` вешается вешается на родительский элемент с классом `<div class="fv__block">`.

***
##### selectLoadImages()

> Используется для загрузки иконок флагов стран в кастомизированный `<select></select>` мультинациональной маски из стандартного поля `<select></select>`. Путь к иконке задается в дата атрибуте data-img элемента `<option data-img="..."></option>`.

Смена иконок происходит в обработчике события change элемента `<select></select>`.

***
##### fileListcontroll()

> Используется для создания списка загружаемых файлов для `<input type="file" multiple>`. Дополнительно с этми нужно разместить блок с классом `<div class="input-file-list"></div>` внутри блока `<div class="fv__block required--file--multiple">`. Использовать совместно с fileQuantity(). Обязательно объявить переменные:

``` 
var	$inputFile = $('.required--file--multiple input[type="file"]'),
	$fileList = $('`<div>input-file-list'),
	$input,
	object = {},
	i,
	j = 0;
```

***
##### fileQuantity()

> Используется для контроля количества элементов в списке загруженых файлов элемента `<input type="file" multiple>`. Принимает в качестве параметра объект с данными загружаемых файлов. Использовать совместно с fileListcontroll()

***
### keyup_form()

> Функция инициализирует все функции выше

***
### click_submit()

> Функция срабатывает по клику на `<input type="submit">` с классом `class="required--sbmt"`. Проверяет все поля по классам выше, в случае ошибок через функцию popupForm__error(count) выводит сообщения с ошибкой. Если ошибок нету то сабмитит форму
