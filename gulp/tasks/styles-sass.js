const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');
module.exports = function () {
	$.gulp.task('styles-sass:build', () => {
		return $.gulp.src('./dev/sass/main.scss')
			.pipe($.gp.sass({
				'include css': true
			}))
			.pipe($.gp.autoprefixer({
				browsers: ['last 10 version']
			}))
			.pipe($.gp.csscomb())
			.pipe($.gp.csso())
			.pipe($.gulp.dest('./build/css/'))
	});

	$.gulp.task('styles-sass:dev', () => {
		return $.gulp.src('./dev/sass/main.scss')
			.pipe($.gp.sourcemaps.init())
			.pipe($.gp.sass({
				'include css': true
			}))
			.on('error', $.gp.notify.onError(function (error) {
				return {
					title: 'Sass',
					message: error.message
				};
			}))

			.pipe($.gp.autoprefixer({
				browsers: ['last 5 version']
			}))
			// .pipe($.gp.cssmin())
			// .pipe($.gp.rename({suffix: '.min'}))
			.pipe($.gp.sourcemaps.write())
			.pipe($.gulp.dest('./build/css/'))
			.pipe($.browserSync.reload({
				stream: true
			}));
	});

	$.gulp.task('css-libs:build', () => {
		return $.gulp
			.src(['./dev/libs/*.css'])
			.pipe($.gp.csscomb())
			.pipe($.gp.csso())
			.pipe($.gulp.dest('./build/libs/'))
	});

	$.gulp.task('css-libs:dev', () => {
		return $.gulp
			.src(['./dev/libs/*.css'])
			.pipe($.gulp.dest('./build/libs/'))
			.pipe(
				$.browserSync.reload({
					stream: true,
				}),
			);
	});

};