const include = require('gulp-include');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');

module.exports = function () {
   $.gulp.task('js:copy:dev', () => {
      return $.gulp
         .src(['./dev/js/*.js'])
         .pipe(
            include({
               includePaths: [
                  __dirname + '/../../node_modules',
                  __dirname + '/../../dev/js',
                  __dirname + '/../../dev/js/libs'
               ],
            }),
         )
         .pipe(babel({
            presets: ['@babel/env']
         }))
         .pipe($.gulp.dest('./build/js/'))
         .pipe(
            $.browserSync.reload({
               stream: true,
            }),
         );
   });


   $.gulp.task('libs:dev', () => {
      return $.gulp
			.src(['./dev/libs/*.js'])
         .pipe($.gulp.dest('./build/libs/'))
         .pipe(
            $.browserSync.reload({
               stream: true,
            }),
         );
   });

   $.gulp.task('libs:build', () => {
      return $.gulp
			.src(['./dev/libs/*.js'])
			.pipe(babel({
            presets: ['@babel/env']
         }))
         .pipe(uglify())
         .pipe($.gulp.dest('./build/libs/'))
   });
};