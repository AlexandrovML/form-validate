module.exports = function () {
    $.gulp.task('watch', function () {
        $.gulp.watch('./dev/pug/**/*.pug', $.gulp.series('pug'));
		  $.gulp.watch('./dev/stylus/**/*.styl', $.gulp.series('styles:dev'));
		  $.gulp.watch('./dev/sass/**/*.scss', $.gulp.series('styles-sass:dev'));
		  $.gulp.watch('./dev/img/svg/*.svg', $.gulp.series('svg'));
		  $.gulp.watch('./dev/libs/*.{css,js}', $.gulp.series('css-libs:dev','libs:dev'));
        $.gulp.watch('./dev/js/**/*.js', $.gulp.series('js:copy:dev'));
        $.gulp.watch(['./dev/img/general/**/*.{png,jpg,gif,svg}',
                     './dev/img/content/**/*.{png,jpg,gif,svg}'], $.gulp.series('img:dev'));
    });
};