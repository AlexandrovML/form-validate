global.$ = {
    path: {
        task: require('./gulp/paths/tasks.js')
    },
	 gulp: require('gulp'),
    del: require('del'),
    fs: require('fs'),
    browserSync: require('browser-sync').create(),
    gp: require('gulp-load-plugins')()
};

$.path.task.forEach(function(taskPath) {
    require(taskPath)();
});

$.gulp.task('dev', $.gulp.series(
	'clean',
	$.gulp.parallel('styles-sass:dev', 'css-libs:dev', 'pug','js:copy:dev', 'libs:dev', 'svg', 'img:dev', 'fonts','svg:copy','all-font:dev', 'all-font-gif:dev', 'temp:dev')));

$.gulp.task('build', $.gulp.series(
	'clean',
	$.gulp.parallel('styles-sass:build', 'css-libs:build', 'pug', 'libs:build', 'js:copy:dev', 'svg', 'img:build', 'fonts','svg:copy')));

$.gulp.task('default', $.gulp.series(
    'dev',
    $.gulp.parallel(
        'watch',
        'serve'
    )
));